<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['enable_online_user_tracking'] = 'y';
$config['enable_hit_tracking'] = 'y';
$config['enable_entry_view_tracking'] = 'y';
$config['dynamic_tracking_disabling'] = '';
$config['is_system_on'] = 'y';
$config['multiple_sites_enabled'] = 'n';
$config['show_ee_news'] = 'n';
// ExpressionEngine Config Items
// Find more configs and overrides at
// https://docs.expressionengine.com/latest/general/system_configuration_overrides.html

$config['app_version'] = '5.1.2';
$config['encryption_key'] = '58cb6a8326f6d1bff93ff04d775db2e55d42c87d';
$config['session_crypt_key'] = '78541cce1580de1d3152b79b006eba4998b48012';
$config['database'] = array(
	'expressionengine' => array(
		'hostname' => 'localhost',
		'database' => 'cordeiro-guindastes',
		'username' => 'root',
		'password' => '',
		'dbprefix' => 'exp_',
		'char_set' => 'utf8mb4',
		'dbcollat' => 'utf8mb4_unicode_ci',
		'port'     => ''
	),
);

// EOF